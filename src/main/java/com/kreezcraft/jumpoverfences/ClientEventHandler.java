package com.kreezcraft.jumpoverfences;

import net.minecraft.client.player.LocalPlayer;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(
        modid = "jumpoverfences",
        value = {Dist.CLIENT}
)
public class ClientEventHandler {
    @SubscribeEvent
    public static void onPlayerJump(LivingEvent.LivingJumpEvent event) {
        if (event.getEntity() instanceof LocalPlayer player) {
            if (player.input.jumping && isPlayerNextToFence(player)) {
                player.setDeltaMovement(player.getDeltaMovement().add(0.0D, 0.05D, 0.0D));
            }
        }

    }

    private static boolean isPlayerNextToFence(LocalPlayer player) {
        double x = player.getX() - 1.0D;
        double y = player.getY();
        double z = player.getZ() - 1.0D;

        for(int i = 0; i < 3; ++i) {
            for(int j = 0; j < 3; ++j) {
                if ((double)i != x || (double)j != z) {
                    Block block = getBlock(player.level, new BlockPos(x + (double)i, y, z + (double)j));
                    if (block instanceof FenceBlock || block instanceof WallBlock) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static Block getBlock(Level world, BlockPos pos) {
        return world.getBlockState(pos).getBlock();
    }
}
