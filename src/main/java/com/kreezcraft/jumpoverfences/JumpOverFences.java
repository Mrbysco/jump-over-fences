package com.kreezcraft.jumpoverfences;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("jumpoverfences")
public class JumpOverFences
{
    static final Logger LOGGER = LogManager.getLogger();

    public JumpOverFences() {
        if(FMLLoader.getDist() == Dist.DEDICATED_SERVER) {
            LOGGER.info("This mod should only be installed on the client to save RAM");
            return;
        }
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
    }

    private void init(FMLClientSetupEvent evt) {
    }
}
